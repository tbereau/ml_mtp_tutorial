#!/usr/bin/env python
# Test file
#
# Tristan Bereau (2015)

from src.calculator import Calculator
from src.multipole_ml_bset import MultipoleMLBSet
import src.utils
from src.system import System
import os, sys, glob, math
import numpy as np
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import glob
from random import shuffle
from scipy.optimize import minimize
import ConfigParser
import argparse

np.set_printoptions(precision=3, suppress=True)

def gather_xyz_txt_files(length, ele=None):
    """Return list of xyz and hipart txt files"""
    isa_col  = glob.glob(r'../../datasets/gdb1-4_ori/*.mtp')
    isa_col += glob.glob(r'../../datasets/gdb1-4_pert/*.mtp')

    shuffle(isa_col)
    isa_col = isa_col[:length]
    xyz_col = []
    hrt_fin = []
    for isa_file in isa_col:
        idxl = isa_file.rfind(".mtp")
        xyz_file = isa_file[0:idxl] + ".xyz"
        if os.path.isfile(xyz_file) and os.path.getsize(xyz_file) > 0 \
            and os.path.isfile(isa_file) and os.path.getsize(isa_file) > 0:
            mol = System(xyz_file)
            # Check that there are only elements N C O and H
            all_ncoh = True
            ele_present = True if ele is None else False
            for at in mol.elements:
                if at not in ["N","C","O","H"]:
                    all_ncoh = False
                if at == ele:
                    ele_present = True
            # Check that no H charge is negative
            no_neg_h_chg = True
            mol.load_mtp_from_ref(isa_file)
            for i in range(mol.num_atoms):
                if mol.elements[i] is "H" and mol.multipoles[i][0] < 0.:
                    no_neg_h_chg = False
                    break
            if all_ncoh and ele_present and no_neg_h_chg:
                xyz_col.append(xyz_file)
                hrt_fin.append(isa_file)
    return xyz_col, hrt_fin


def separate_training_predict(xyz_list, hipart_list, frac_training):
    """Separate training/predict list with frac_training ratio"""
    idx_shuf = range(len(xyz_list))
    listxyz = []
    listtxt = []
    # shfd = shuffle(mol_list)
    shuffle(idx_shuf)
    if frac_training < 0. or frac_training > 1.:
        print "Error: training fraction must lie between 0 and 1."
        exit(1)
    frac_num = int(math.floor(frac_training * len(xyz_list)))
    for i in idx_shuf:
        listxyz.append(xyz_list[i])
        listtxt.append(hipart_list[i])
    return listxyz[:frac_num],listxyz[frac_num:], \
            listtxt[:frac_num],listtxt[frac_num:]

def train_ml(xyz_training, mtp_training, ele=None, save=None, load=None, hyperparams=None):
    """Train machine learning on data set"""
    calc = Calculator()
    mtp_model = MultipoleMLBSet(calc)
    if hyperparams != None:
        assign_params(hyperparams, mtp_model)
    if load != None:
        for l in load:
            mtp_model.load_ml(l)
    else:
        ele_set = set([])
        for xyz, mtp in zip(xyz_training, mtp_training):
            mol1 = System(xyz)
            mtp_model.add_mol_to_training(mol1, mtp, ele)
            # for i in xrange(len(mol1.coulomb_mat)):
            #     print mtp_model.target_train[i]
            #     print mol1.coulomb_mat[i]
        mtp_model.train_mol()
        if save != None:
            mtp_model.save_ml(save)
    if ele is not None:
        print "# Training:",len(mtp_model.target_train[ele]),"atoms,",mtp_model.num_mols_train[ele],"molecules"
    else:
        for e in ["H","C","O","N"]:
            print "# Training:",len(mtp_model.target_train[e]),"atoms,",mtp_model.num_mols_train[e],"molecules"
    return mtp_model

def predict_ml(mtp_model, xyz_predict, mtp_predict, element=None):
    """Predict list of molecules using mtp_model"""
    mtp_ref = []
    mtp_ml  = []
    elements = []
    num_pred = 0
    for xyz, mtp in zip(xyz_predict, mtp_predict):
        mol1 = System(xyz=xyz)
        # Read in hipart file
        mol1.load_mtp_from_ref(mtp)
        mtp_ref += mol1.multipoles
        ref_cur = mol1.multipoles
        mol1 = System(xyz=xyz)
        # Now predict multipoles
        mtp_model.predict_mol(mol1, charge=0)
        mtp_ml += mol1.multipoles.tolist()
        elements += mol1.elements
        # for i in range(mol1.num_atoms):
        #     if mol1.elements[i] is element:
        #         if abs(mol1.multipoles[i][4] - ref_cur[i][4]) > 0.3:
        #             print "..",mol1,mol1.multipoles[i][4], ref_cur[i][4]
    for i,j,k in zip(mtp_ml,mtp_ref,elements):
        if k is element or element is None:
            for idx in xrange(9):
                print "%2s %7.4f %7.4f   " % (k,i[idx],j[idx]),
            print ""
            num_pred += 1
    mae_avg = np.zeros(3)

    for at in set(["N","C","O","H"]):
        if len(mtp_model.descr_train[at]) > 0:
            print "# Correlations %2s (%5d): " % (at, sum([1 for att in elements if att==at])),
            for idx in xrange(0,9):
                r2 = pearsonr([ele[idx] for ele,att in zip(mtp_ml,elements) if att==at],
                        [ele[idx] for ele,att in zip(mtp_ref,elements) if att==at])
                mae = np.mean([abs(ml[idx]-ref[idx]) for ml,ref,att
                        in zip(mtp_ml,mtp_ref,elements) if att==at])
                if idx is 0:
                    mae_avg[0] += mae
                elif idx in [1,2,3]:
                    mae_avg[1] += mae/3.
                else:
                    mae_avg[2] += mae/5.
                print "%7.4f (%7.4f) " % (r2[0], mae),
            print ""
    print "# Prediction on",num_pred,"atoms",len(xyz_predict),"molecules"
    # plot(mtp_ml,mtp_ref,elements, element)
    return mae_avg

def plot(predict, target, elements, element):
    # plt.plot(predict,target,'ro')
    ncoh = ['N','C','O','H']
    for idx in range(0,9):
        plt.plot([pred[idx] for pred,ele in zip(predict,elements) if ele is element],
            [tgt[idx] for tgt,ele in zip(target,elements) if ele is element],'ro',
            color=plt.cm.RdYlBu(idx/9.), alpha=0.6)
    plt.plot([-1,1],[-1,1],'-')
    return plt.show()

def assign_params(hp, mtp_model):
    """Assign hyperparameters to config file"""
    mtp_model.krr_sigma = abs(hp[0])
    return None

def train_and_predict(hyperparams, xyz_training, mtp_training, xyz_predict,
        mtp_predict, save=None, load=None):
    mtp_model = train_ml(xyz_training, mtp_training, save=None, load=None, hyperparams=hyperparams)
    mae_avg = predict_ml(mtp_model, xyz_predict, mtp_predict)
    # print("Parameters %12.4f %12.4f %12.4f - R^2: %7.4f" % (hyperparams[0],
    #         hyperparams[1], hyperparams[2], r2_avg))
    # print("Parameters %12.4f %12.4f - R^2: %7.4f" % (hyperparams[0], hyperparams[1], r2_avg))
    print("Parameters %12.4f - MAE: %7.4f" % (hyperparams[0], mae_avg))
    return mae_avg


def run_ml(length, frac_training, ele=None, save=None, load=None):
    """Run training/prediction with frac_training ratio"""
    xyz_col, hipart_col = gather_xyz_txt_files(length, ele)
    xyz_training, xyz_predict, mtp_training, mtp_predict = \
        separate_training_predict(xyz_col, hipart_col, frac_training)
        # Optimize hyperparameters
    # x0 = [10.0]
    # res = minimize(train_and_predict, x0, args=(xyz_training, mtp_training, xyz_predict, mtp_predict),
    #         method='nelder-mead', options={'xtol': 1e-2, 'disp': True})
    #         # options={'lower': 0.0, 'boltzmann': 1.0, 'schedule': 'boltzmann',
    #         #     'disp': True})
    # print(res.x)
    # for sig in [1.0, 10.0, 100.0, 1000.0, 10000.0]:
    #     hyperparams = [sig]
    #     train_and_predict(hyperparams, xyz_training, mtp_training, xyz_predict,
    #             mtp_predict)
    mtp_model = train_ml(xyz_training, mtp_training, ele, save, load)
    mae_avg = predict_ml(mtp_model, xyz_predict, mtp_predict, ele)
    return mae_avg

def main():
    parser = argparse.ArgumentParser(description='Train and predict covariant multipoles.')
    parser.add_argument('--mols', metavar='mols', type=int, required=True,
                        help='maximum number of molecules')
    parser.add_argument('--frac', metavar='fraction', type=float, required=True,
                        help='training fraction')
    parser.add_argument('--ele', metavar='ele', type=str,
                        help='chemical element')
    parser.add_argument('--save', metavar='save', type=str, required=False,
                        help='save pkl file')
    parser.add_argument('--load', metavar='load', nargs='+', type=str, required=False,
                        help='load pkl file')
    parser.add_argument('--cycles', metavar='cycles', type=int, required=False, default=1)
    args = parser.parse_args()

    mae_avg = np.zeros(3)
    num_cycles = 5
    for i in range(args.cycles):
        mae_avg += run_ml(args.mols, args.frac, ele=args.ele, save=args.save, load=args.load)
    mae_avg /= args.cycles
    print "# MAEl0:{:9.5f}\n# MAEl1:{:9.5f}\n# MAEl2:{:9.5f}".format(
        mae_avg[0], mae_avg[1], mae_avg[2])
    return None

if __name__ == '__main__':
    main()
