# Tutorial: Machine learning of multipole electrostatic coefficients

======

Tristan Bereau
Max Planck Institute for Polymer Research
bereau@mpip-mainz.mpg.de

======

## Installation

1. Extract the dataset: `tar -xzf datasets.tgz`.
