#!/usr/bin/env python
#
# Multipoles_ml_covar class. Predict multipole parameters from ML.
# No local axis system. Build a covariant kernel matrix.
# (Glielmo, Sollich, De Vita, arXiv 1611.03877v1)
#
# Tristan Bereau (2016)

from electrostatics import Electrostatics
from system import System
import scipy
import numpy as np
import logging
import pickle
import constants
import math
import utils
import os
import sys
from scipy.spatial.distance import pdist, cdist, squareform

# Set logger
logger = logging.getLogger(__name__)

class MultipoleMLCovar(Electrostatics):
    '''
    MultipoleMLCovar class. Predicts multipoles from machine learning.
    No local axis system. Instead, rely on covariant kernel with overlap
    of atom-centered Gaussian functions.
    '''

    def __init__(self, _calculator):
        Electrostatics.__init__(self, _calculator)
        self.mol_train = {0:{'H':[], 'C':[], 'O':[], 'N':[]},
                          1:{'H':[], 'C':[], 'O':[], 'N':[]},
                          2:{'H':[], 'C':[], 'O':[], 'N':[]}}
        logger.setLevel(self.calculator.get_logger_level())
        self.max_neighbors = self.calculator.Config.getint( \
            "multipoles","max_neighbors")
        self.alpha_train = {0:[], 1:{}, 2:{}}
        self.max_coeffs = [1, 3, 9]
        self.offset_mtp = [0, 1, 4]
        self.kernel = self.calculator.Config.get( \
            "multipoles","kernel")
        self.krr_sigma = self.calculator.Config.getfloat( \
            "multipoles","krr_sigma")
        self.krr_lambda = self.calculator.Config.getfloat( \
            "multipoles","krr_lambda")
        self.num_mols_train = {0:{'H':0, 'C':0, 'O':0, 'N':0},
                               1:{'H':0, 'C':0, 'O':0, 'N':0},
                               2:{'H':0, 'C':0, 'O':0, 'N':0}}
        self.num_ats_train  = {0:{'H':0, 'C':0, 'O':0, 'N':0},
                               1:{'H':0, 'C':0, 'O':0, 'N':0},
                               2:{'H':0, 'C':0, 'O':0, 'N':0}}
        # Map molecule to atom in training set
        self.num_elements = {0:{'H':0, 'C':0, 'O':0, 'N':0},
                             1:{'H':0, 'C':0, 'O':0, 'N':0},
                             2:{'H':0, 'C':0, 'O':0, 'N':0}}
        self.target_train = {0:{'H':[], 'C':[], 'O':[], 'N':[]},
                             1:{'H':[], 'C':[], 'O':[], 'N':[]},
                             2:{'H':[], 'C':[], 'O':[], 'N':[]}}

    def load_ml(self, load_file=None):
        '''Load machine learning model'''
        # Try many atoms and see which atoms we find
        if load_file != None:
            logger.info(
                    "Reading multipole training from %s" % load_file)
            with open(load_file, 'rb') as f:
                mol_train, alpha_train, \
                    target_train, num_elements = pickle.load(f)
            for rk in self.num_elements.keys():
                for e in self.num_elements[rk].keys():
                    if num_elements[rk][e] > 0:
                        # Update rk and e
                        self.mol_train[rk][e] = mol_train[rk][e]
                        if rk == 0:
                            self.alpha_train[0] = alpha_train[0]
                        else:
                            self.alpha_train[rk][e] = alpha_train[rk][e]
                        self.target_train[rk][e] = target_train[rk][e]
                        self.num_elements[rk][e] = num_elements[rk][e]
        else:
            logger.error("Missing load file name")
            exit(1)
        return None

    def save_ml(self, save_file):
        '''Save machine learning model'''
        logger.info("Saving multipole machine learning model to %s" %
            save_file)
        with open(save_file, 'w') as f:
            pickle.dump([self.mol_train, self.alpha_train,
                         self.target_train, self.num_elements],
                    f, protocol=2)
        return None

    def train_mol(self, rk):
        '''Train machine learning model for multipoles of rank rk.'''
        dim = self.max_coeffs[rk]
        alpha_train = np.empty(3)
        if rk == 0:
            size_training = len(self.target_train)
            logger.info("Training set size: %d atoms; %d molecules" % (sum([n for n in self.num_ats_train[0].values()]),
                                                                       sum([n for n in self.num_mols_train[0].values()])))
            coulmats = [mi.coulomb_mat[at]
                        for j in sorted(self.num_elements[0].keys())
                        for mi in self.mol_train[0][j]
                        for at in range(mi.num_atoms)
                        if mi.elements[at] == j]
            pairwise_dists = squareform(pdist(coulmats,
                                              constants.ml_metric[self.kernel]))
            logger.info("building kernel matrix of size (%d,%d); %7.4f Gbytes" \
                        % (size_training, size_training, 8*size_training**2/1e9))
            power  = constants.ml_power[self.kernel]
            prefac = constants.ml_prefactor[self.kernel]
            kernel = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
            # Target for monopole
            target = [mi[0]
                      for j in sorted(self.num_elements[0].keys())
                      for mi in self.target_train[0][j]]
            kernel += self.krr_lambda*np.identity(len(kernel[0]))
            alpha_train = np.linalg.solve(kernel, target)
            self.alpha_train[rk] = alpha_train
        else:
            size_training = len(self.target_train[rk])
            if rk not in self.alpha_train.keys():
                self.alpha_train[rk] = {}
            for e in self.num_elements[rk].keys():
                logger.info("Training set size: %d atoms; %d molecules" % (self.num_ats_train[rk][e],
                                                                           self.num_mols_train[rk][e]))
                if e not in self.alpha_train[rk].keys():
                    self.alpha_train[rk][e] = {}
                mol_at = []
                for mi in range(len(self.mol_train[rk][e])):
                    for i in range(self.mol_train[rk][e][mi].num_atoms):
                        if self.mol_train[rk][e][mi].elements[i] == e:
                            mol_at.append([mi, i])
                print "#",e,rk,"",
                target = np.zeros((dim*self.num_elements[rk][e]))
                for i in range(self.num_elements[rk][e]):
                    if rk < 2:
                        target[dim*i:dim*i+dim] = self.target_train[rk][e][i][
                            self.offset_mtp[rk]:self.offset_mtp[rk]+dim]
                    else:
                        target[dim*i:dim*i+dim] = utils.spher_to_cart(self.target_train[rk][e][i][
                            self.offset_mtp[rk]:self.offset_mtp[rk]+dim]).flatten()
                kernel = np.zeros((dim*self.num_elements[rk][e], dim*self.num_elements[rk][e]))
                for i in range(len(mol_at)):
                    print i,
                    sys.stdout.flush()
                    for j in range(len(mol_at)):
                        kernel[dim*i:dim*i+dim,dim*j:dim*j+dim] = utils.compare_two_atomic_envs(
                            self.mol_train[rk][e][mol_at[i][0]], self.mol_train[rk][e][mol_at[j][0]],
                            mol_at[i][1], mol_at[j][1], rk)
                if len(kernel) > 0:
                    kernel += self.krr_lambda*np.identity(len(kernel[0]))
                    alpha_train = np.linalg.solve(kernel, target)
                    self.alpha_train[rk][e] = alpha_train
        logger.info("training of multipoles finished.")
        return None

    def predict_mol(self, _system, charge=0):
        '''Predict multipoles in local reference frame given descriptors.'''
        _system.initialize_multipoles()
        _system.build_coulomb_matrices(self.max_neighbors)
        _system.initialize_atomic_environment()
        _system.multipoles = np.zeros((_system.num_atoms,9))
        # Molecule is prediction set
        self.at_typ_pred = []
        for i in xrange(_system.num_atoms):
            self.at_typ_pred.append(_system.elements[i])
        # monopole
        if len(self.alpha_train[0]) > 0:
            coulmats = [mi.coulomb_mat[at]
                        for j in sorted(self.num_elements[0].keys())
                        for mi in self.mol_train[0][j]
                        for at in range(mi.num_atoms)
                        if mi.elements[at] == j]
            pairwise_dists = cdist(_system.coulomb_mat, \
                                   coulmats, constants.ml_metric[self.kernel])
            power  = constants.ml_power[self.kernel]
            prefac = constants.ml_prefactor[self.kernel]
            kernel = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
            pred = np.dot(kernel,self.alpha_train[0])
            _system.multipoles[:,0] = pred

        # Higher multipoles (rank 1 and 2)
        for rk in [1,2]:
            for e in self.num_elements[rk].keys():
                # One set of optimized coefficients alpha per element
                at_pred_e = [i for i in range(len(self.at_typ_pred))
                             if self.at_typ_pred[i] == e]
                num_pred = len(at_pred_e)
                mol_at = []
                for mi in range(len(self.mol_train[rk][e])):
                    for i in range(self.mol_train[rk][e][mi].num_atoms):
                        if self.mol_train[rk][e][mi].elements[i] == e:
                            mol_at.append([mi, i])
                if rk in self.alpha_train.keys():
                    if e in self.alpha_train[rk].keys():
                        dim = self.max_coeffs[rk]
                        kernel = np.zeros((dim*num_pred, dim*self.num_elements[rk][e]))
                        for i in range(num_pred):
                            for j in range(len(mol_at)):
                                kernel[dim*i:dim*i+dim,dim*j:dim*j+dim] = utils.compare_two_atomic_envs(
                                    _system, self.mol_train[rk][e][mol_at[j][0]],
                                    at_pred_e[i], mol_at[j][1], rk)
                        pred = np.dot(kernel,self.alpha_train[rk][e])
                        for i,idx in enumerate(at_pred_e):
                            pred_i = pred[self.max_coeffs[rk]*i:self.max_coeffs[rk]*(i+1)]
                            if self.num_elements[rk][e] > 0:
                                if rk == 2:
                                    pred_i = utils.cart_to_spher(pred_i.reshape((3,3)))
                                _system.multipoles[idx,self.offset_mtp[rk]:self.offset_mtp[rk]+self.max_coeffs[rk]] = pred_i
        # Correct to get integer charge
        if self.calculator.Config.get("multipoles","correct_charge").lower() in \
            ['true','yes','1']:
            totalcharge = sum([mtp[0] for mtp in _system.multipoles])
            abscharge   = sum([abs(mtp[0]) for mtp in _system.multipoles])
            excess_chg = totalcharge - float(charge)
            if abscharge != 0.0:
                for i,mtp_i in enumerate(_system.multipoles):
                    mtp_i[0] += -1.*excess_chg * abs(mtp_i[0])/abscharge
        logger.debug("Predicted multipole expansion for %s" % ( _system))
        return None

    def add_mol_to_training(self, new_system, ref, rank, atom=None):
        'Add molecule to training set. If atom != None, filter by elements atom.'
        new_system.initialize_multipoles()
        new_system.build_coulomb_matrices(self.max_neighbors)
        new_system.initialize_atomic_environment()
        new_system.multipoles = np.empty((new_system.num_atoms,9))
        # Read in multipole moments from DMA or hipart txt file
        new_system.load_mtp_from_hipart(ref)
        num_new_ats = 0
        for i in xrange(len(new_system.elements)):
            ele_i = new_system.elements[i]
            if ele_i is atom or atom is None:
                if ele_i not in self.target_train[rank].keys():
                    self.target_train[rank][ele_i] = []
                    self.num_elements[rank][ele_i] = 0
                self.target_train[rank][ele_i].append(new_system.multipoles[i])
                self.num_elements[rank][ele_i] += 1
                self.num_ats_train[rank][ele_i] += 1
        if atom in new_system.elements:
            if atom not in self.mol_train[rank].keys():
                self.mol_train[rank][atom] = []
            self.mol_train[rank][atom].append(new_system)
            self.num_mols_train[rank][atom] += 1
        elif atom is None:
            for a in set(new_system.elements):
                if a not in self.mol_train[rank].keys():
                    self.mol_train[rank][a] = []
                self.mol_train[rank][a].append(new_system)
        logger.info("Added file to training set: %s" % new_system)
        return None
