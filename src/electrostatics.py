#!/usr/bin/env python
#
# Electrostatics class. Compute multipole electrostatic interactions.
#
# Tristan Bereau (2015)

from system import System
import logging

# Set logger
logger = logging.getLogger(__name__)

class Electrostatics:
    'Electrostatics class. Computes electrostatic interactions.'

    def __init__(self, _calculator):
        self.calculator = _calculator
        logger.setLevel(self.calculator.get_logger_level())
        self.multipoles = None
