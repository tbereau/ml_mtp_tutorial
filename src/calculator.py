#!/usr/bin/env python
#
# Calculator class. Initialize calculator.
#
# Tristan Bereau (2015)

import logging
import utils
import ConfigParser

class Calculator:
    'Main calculator'
    # Config parser
    Config = ConfigParser.ConfigParser()
    Config
    # Set logger
    logger = logging.getLogger(__name__)
    logging.basicConfig()

    def __init__(self, config_file="config.ini"):
        # Load config file
        self.Config.read(config_file)
        # Logger level
        self.logger.setLevel(self.get_logger_level())
        utils.set_logger_level(self.get_logger_level())
        self.energy = 0.0

    def get_logger_level(self):
        # Return logger level
        return self.Config.getint("output", "log_level")
