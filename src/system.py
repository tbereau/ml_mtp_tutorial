#!/usr/bin/env python
#
# System class. Define overall molecular system, load coordinates,
# all variables and physical parameters.
#
# Tristan Bereau (2015)

import numpy as np
import utils
import logging
from calculator import Calculator
import constants
import copy
import re

# Set logger
logger = logging.getLogger(__name__)

class System(Calculator):
    'Common system class for molecular system'

    def __init__(self, xyz):
        Calculator.__init__(self, config_file = "config.ini")
        logger.setLevel(self.get_logger_level())
        self.xyz = xyz
        # Coordinates
        self.coords = None
        # Number of atoms in the molecule
        self.num_atoms = 0
        # Chemical elements
        self.elements = None
        # Coulomb matrix
        self.coulomb_mat = None
        self.atom_reorder = None
        # Coulomb matrix and derivatives
        self.coulmat_grads = None
        # Bag of bonds
        self.bag_of_bonds = None
        # multipoles
        self.multipoles = []
        # Expansion coefficients of multipoles along pairwise vectors
        self.mtp_expansion = None
        # Basis set for expansion
        self.basis = []
        # Voronoi baseline for MTPs
        self.voronoi_baseline = []
        # Principal axes for MTPs
        self.principal_axes = []
        # Pairwise vectors and rotation matrices for atomic environment descriptor
        self.pairwise_vec = []
        self.pairwise_norm = []
        self.rot_mat = []
        self.load_xyz()

    def __add__(self, sys):
        """Combine two systems"""
        s = copy.deepcopy(self)
        s.elements = self.elements + sys.elements
        s.num_atoms = self.num_atoms + sys.num_atoms
        s.coords = np.append(s.coords,sys.coords).reshape((s.num_atoms,3))
        s.atom_types = self.atom_types + sys.atom_types
        return s

    def __str__(self):
        if self.xyz:
            return self.xyz
        elif self.mps:
            return self.mps
        elif self.dma:
            return self.dma
        else:
            logger.error("No molecule name!")
            exit(1)

    def load_xyz(self):
        extract_file = utils.read_file(self.xyz)
        self.num_atoms = int(extract_file[0])
        self.elements = [str(line.split()[0])
                        for i,line in enumerate(extract_file)
                        if i>1 and i<self.num_atoms+2]
        iterable = (float(line.split()[j])
                        for i,line in enumerate(extract_file)
                        for j in range(1,4)
                        if i>1 and i<self.num_atoms+2)
        self.coords = np.fromiter(iterable,
                    np.float).reshape(self.num_atoms, 3)
        self.logger.debug('Loaded molecule %s with %s atoms.' \
            % (self.xyz, self.num_atoms))
        self.logger.debug('Elements %s' % ', '.join(self.elements))
        return None

    def build_coulomb_matrices(self, max_neighbors, direction=None):
        self.coulomb_mat = []
        self.atom_reorder = []
        for at in xrange(len(self.elements)):
            coul_mat, reorder_atoms = utils.build_coulomb_matrix(
                self.coords, self.elements, at, max_neighbors, direction)
            self.coulomb_mat.append(coul_mat)
            self.atom_reorder.append(reorder_atoms)
        return None

    def build_coulomb_grads(self, max_neighbors):
        self.coulmat_grads = []
        self.atom_reorder = []
        for at in xrange(len(self.elements)):
            coulmat0, coulmat1, coulmat2, reorder_atoms = \
                utils.coulomb_with_grads(self.coords, self.elements,
                at, max_neighbors)
            self.coulmat_grads.append([coulmat0,coulmat1,coulmat2])
            self.atom_reorder.append(reorder_atoms)
        return None

    def build_bag_of_bonds(self, bob_struct, max_neighbors):
        self.bag_of_bonds = []
        for at in xrange(len(self.elements)):
            bob, reorder = utils.build_bag_of_bonds(self.coords, self.elements, \
                at, bob_struct, max_neighbors)
            self.bag_of_bonds.append(bob)
            if len(self.atom_reorder) < self.num_atoms:
                self.atom_reorder.append(reorder)
        return None


    def initialize_multipoles(self):
        '''Initialize multipoles to 0'''
        self.multipoles = np.zeros((self.num_atoms,9))
        # mtp_expansion has size 1+3+9=13 for ranks 0,1,2.
        self.mtp_expansion = np.zeros((self.num_atoms,13))
        # MTPs based on derivatives. 1+3+6
        self.multipoles_grads = np.zeros((self.num_atoms,10))
        return None

    def initialize_atomic_environment(self):
        '''Store pairwise vectors and rotation matrices for
        atomic environment descriptor'''
        self.pairwise_vec = []
        self.pairwise_norm = []
        self.rot_mat = []
        z = np.array([0.,0.,1.])
        for i in range(self.num_atoms):
            pair_i = []
            pair_r_i = []
            rot_i = []
            for j in range(self.num_atoms):
                v = self.coords[j] - self.coords[i]
                pair_i.append(v)
                pair_r_i.append(np.linalg.norm(v))
                rot_i.append(utils.ab_rotation(v, z))
            self.pairwise_vec.append(pair_i)
            self.pairwise_norm.append(pair_r_i)
            self.rot_mat.append(rot_i)
        return None

    def compute_voronoi(self):
        '''Estimates MTP coefficients from Voronoi for atom atom (ID) on a discrete
        set of points.'''
        self.voronoi_baseline = []
        if self.Config.get("multipoles","voronoi") in \
            ["on","yes","True","true"]:
            a2b = constants.a2b
            grid_max = self.Config.getfloat("multipoles","voronoi_grid_max")
            grid_step = self.Config.getfloat("multipoles","voronoi_grid_step")
            # Work in bohr
            b_coords = self.coords*a2b
            for i in xrange(len(self.elements)):
                # Grid-point boundaries
                xmin = b_coords[i][0] - grid_max
                ymin = b_coords[i][1] - grid_max
                zmin = b_coords[i][2] - grid_max
                xmax = xmin + 2*grid_max
                ymax = ymin + 2*grid_max
                zmax = zmin + 2*grid_max
                # Initialize vector of coefficients
                v_coeffs = np.zeros(10)
                x0 = xmin
                weightar = []
                while x0 < xmax:
                    y0 = ymin
                    while y0 < ymax:
                        z0 = zmin
                        while z0 < zmax:
                            pos  = np.array([x0,y0,z0])
                            rvec = np.array(pos-b_coords[i])
                            n_a_free = np.ones(10)* \
                                utils.atom_dens_free(b_coords[i], \
                                    self.elements[i], pos, i)
                            dist_term = np.array([1.,rvec[0],rvec[1],rvec[2],
                                rvec[0]*rvec[0],rvec[0]*rvec[1],rvec[0]*rvec[2],
                                rvec[1]*rvec[1],rvec[1]*rvec[2],rvec[2]*rvec[2]])
                            fac = np.multiply(dist_term, n_a_free)
                            # Voronoi
                            closest_atm = i
                            shortest_dis = 1000.0
                            for atomj in range(self.num_atoms):
                                atom_dis = np.linalg.norm(pos-b_coords[atomj])
                                if atom_dis < shortest_dis:
                                    closest_atm = atomj
                                    shortest_dis = atom_dis
                            if closest_atm == i:
                                v_coeffs += fac
                            # Update coordinates
                            z0 += grid_step
                        y0 += grid_step
                    x0 += grid_step
                # Convert to spherical coordinates
                quad_cart = np.array([v_coeffs[4],v_coeffs[5],v_coeffs[6],
                   0.,v_coeffs[7],v_coeffs[8],0.,0.,v_coeffs[9]]).reshape(3,3)
                quad_sph  = utils.cart_to_spher(quad_cart)
                self.voronoi_baseline.append([v_coeffs[0], \
                    np.array([v_coeffs[1],v_coeffs[2],v_coeffs[3]]), \
                    quad_sph])
                logger.debug("Voronoi coeffs for atom %s (ID: %d):\n %s" % \
                    (self.elements[i],i, \
                    np.hstack([v_coeffs[0], \
                    np.array([v_coeffs[1],v_coeffs[2],v_coeffs[3]]), \
                    quad_sph])))
        else:
             for i in xrange(len(self.elements)):
                 self.voronoi_baseline.append([0., np.zeros(3), np.zeros(5)])
        return None

    def compute_principal_axes(self):
        '''Project MTP coefficients (except for Q00) along each atom-atom vector.
        Ordered by atom ID. Returns  principal axes.'''
        self.principal_axes = []
        mass = np.zeros(len(self.elements))
        for m in xrange(len(self.elements)):
            mass[m] = constants.atomic_weight[self.elements[m]]
        for i in xrange(len(self.coords)):
            atomi = self.coords[i]
            eigvecs = np.zeros((3,3))
            if self.num_atoms == 1:
                eigvecs = np.identity(3)
            elif self.num_atoms == 2:
                # Only one axis defined
                eigvecs[:,0] = self.coords[i]
                for j in xrange(len(self.coords)):
                    # Only take neighbor
                    if j==i+1:
                        eigvecs[:,0] -= self.coords[j]
                if abs(np.linalg.norm(eigvecs[:,0])) > 1e-12:
                    eigvecs[:,0] /= np.linalg.norm(eigvecs[:,0])
                # Construct other two eigenvectors
                eigvecs[:,1] = np.cross([1,1,1],eigvecs[:,0])
                if abs(np.linalg.norm(eigvecs[:,1])) > 1e-12:
                    eigvecs[:,1] /= np.linalg.norm(eigvecs[:,1])
                eigvecs[:,2] = np.cross(eigvecs[:,0],eigvecs[:,1])
                if abs(np.linalg.norm(eigvecs[:,2])) > 1e-12:
                    eigvecs[:,2] /= np.linalg.norm(eigvecs[:,2])
                # center of mass of molecule
                com = sum([mass[j]*self.coords[j] for j in \
                    xrange(len(self.coords))]) / sum(mass)
                for v in xrange(3):
                    if np.dot(com - self.coords[i],eigvecs[v]) < 0.:
                      eigvecs[v] *= -1.
            else:
                # Compute inertia tensor of molecule around atom i
                coords = self.coords - self.coords[i]
                inertia = np.dot(mass*coords.transpose(),coords)
                eigvals,eigvecs = np.linalg.eig(inertia)
                # center of mass of molecule
                com = sum([mass[j]*self.coords[j] for j in \
                    xrange(len(self.coords))]) / sum(mass)
            # Orient first two eigenvectors so that com is in positive quadrant.
            for v in xrange(3):
                if np.dot(com - self.coords[i],eigvecs[v]) < 0.:
                  eigvecs[v] *= -1.
            self.principal_axes.append(eigvecs.transpose())
            logger.debug("Principal axes for atom %s (ID: %d):\n %s" % \
                (self.elements[i],i,self.principal_axes[i]))
        return None

    def load_mtp_from_ref(self, txt):
        """Load multipoles from hipart output text file"""
        extract_file = utils.read_file(txt)
        self.multipoles = [np.array([
                        float(extract_file[i].split()[4]),
                        float(extract_file[i].split()[6]),
                        float(extract_file[i].split()[7]),
                        float(extract_file[i].split()[5]),
                        float(extract_file[i].split()[8]),
                        float(extract_file[i].split()[9]),
                        float(extract_file[i].split()[10]),
                        float(extract_file[i].split()[11]),
                        float(extract_file[i].split()[12])])
                            for i in xrange(4,len(extract_file))]
        return None

    def molecular_principal_components(self):
        """
        Principal components around center of mass.
        Returns sorted eigenvalues and eigenvectors.
        """
        return utils.inertia_tensor(self.coords, self.elements)

    def expand_multipoles(self):
        """
        Expand coefficients along basis set to compute multipoles
        """
        self.multipoles = np.zeros((self.num_atoms,9))
        for i in xrange(self.num_atoms):
            self.multipoles[i][0] = self.mtp_expansion[i][0]
            # dipole
            if np.linalg.norm(self.mtp_expansion[i][1:4]) > 0.:
                dip = np.dot(self.mtp_expansion[i][1:4], self.basis[i])
                for j in xrange(3):
                    self.multipoles[i][1+j] = dip[j]
            # quadrupole
            if np.linalg.norm(self.mtp_expansion[i][4:]) > 0.:
                quadloc = self.mtp_expansion[i][4:].reshape((3,3))
                quad = utils.cart_to_spher(np.dot(np.dot(
                        self.basis[i].T,
                    self.mtp_expansion[i][4:].reshape((3,3))),self.basis[i]),
                    stone_convention=True)
                # quadloc = utils.spher_to_cart(self.mtp_expansion[i][4:])
                # quad = utils.cart_to_spher(np.dot(np.dot(
                #         np.linalg.inv(self.basis[i]), quadloc),
                #         self.basis[i]), stone_convention=True)
                for j in xrange(5):
                    self.multipoles[i][4+j] = quad[j]
        return None

    def compute_basis(self):
        """
        Basis for multipole expansion
        """
        self.basis = []
        vec_all_dir = []
        for i in xrange(self.num_atoms):
            bas, vec = utils.neighboring_vectors(self.coords,
                self.elements, i)
            self.basis.append(bas)
            vec_all_dir.append(vec)
        return vec_all_dir
