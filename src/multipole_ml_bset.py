#!/usr/bin/env python
#
# Multipoles_ml_bset class. Predict multipole parameters from ML.
# No local axis system. Instead, basis set expansion along the pairwise vectors.
#
# Tristan Bereau (2016)

from electrostatics import Electrostatics
from system import System
import scipy
from scipy import stats
from scipy.spatial.distance import pdist, cdist, squareform
from sklearn.svm import SVR
import numpy as np
import logging
import pickle
import constants
import math
import utils
import os
import copy

# Set logger
logger = logging.getLogger(__name__)

class MultipoleMLBSet(Electrostatics):
    '''
    MultipoleMLBSet class. Predicts multipoles from machine learning.
    No local axis system. Instead, basis set expansion along the pairwise vectors.
    '''

    def __init__(self, _calculator):
        Electrostatics.__init__(self, _calculator)
        self.descr_train = {'H':[], 'C':[], 'O':[], 'N':[]}
        self.target_train = {'H':[], 'C':[], 'O':[], 'N':[]}
        logger.setLevel(self.calculator.get_logger_level())
        self.max_neighbors = self.calculator.Config.getint( \
            "multipoles","max_neighbors")
        # alpha_train has size 1,3,9
        self.max_coeffs = [1, 3, 9]
        self.offset_mtp = [0, 1, 4]
        self.alpha_train = {'H':None, 'C':None, 'O':None, 'N':None}
        self.kernel = self.calculator.Config.get( \
            "multipoles","kernel")
        self.krr_sigma = self.calculator.Config.getfloat( \
            "multipoles","krr_sigma")
        self.krr_lambda = self.calculator.Config.getfloat( \
            "multipoles","krr_lambda")
        # Normalization of the target data - mean and std for each MTP component
        self.norm_tgt_mean = {'H':np.zeros((3)),
            'C':np.zeros((3)), 'O':np.zeros((3)), 'N':np.zeros((3))}
        self.norm_tgt_std  = {'H':np.ones((3)),
            'C':np.ones((3)), 'O':np.ones((3)), 'N':np.ones((3))}
        self.num_mols_train = {'H':0, 'C':0, 'O':0, 'N':0}

    def load_ml(self, load_file=None):
        '''Load machine learning model'''
        # Try many atoms and see which atoms we find
        if load_file != None:
            logger.info(
                    "Reading multipole training from %s" % load_file)
            with open(load_file, 'rb') as f:
                descr_train_at, alpha_train, norm_tgt_mean, \
                    norm_tgt_std = pickle.load(f)
                for e in self.descr_train.keys():
                    if e in descr_train_at.keys() and len(descr_train_at[e]) > 0:
                        # Update
                        self.descr_train[e] = descr_train_at[e]
                        self.alpha_train[e] = alpha_train[e]
                        self.norm_tgt_mean[e] = norm_tgt_mean[e]
                        self.norm_tgt_std[e] = norm_tgt_std[e]
        else:
            logger.error("Missing load file name")
            exit(1)
        return None

    def save_ml(self, save_file):
        '''Save machine learning model'''
        logger.info("Saving multipole machine learning model to %s" %
            save_file)
        with open(save_file, 'w') as f:
            pickle.dump([self.descr_train, self.alpha_train,
                self.norm_tgt_mean, self.norm_tgt_std], f, protocol=2)
        return None

    def train_mol(self):
        '''Train machine learning model of multipole rank mtp_rank and
        basis set expansion coefficient coeff.'''
        for e in  self.descr_train.keys():
            size_training = len(self.target_train[e])
            # self.normalize(e)
            if len(self.descr_train[e]) > 0:
                logger.info("Training set size: %d atoms; %d molecules" % (size_training,
                    self.num_mols_train[e]))
                tgt_prop = [[self.target_train[e][i][mtp_rank][coeff]
                            for mtp_rank in xrange(3)
                            for coeff in xrange(self.max_coeffs[mtp_rank])]
                            for i in xrange(len(self.target_train[e]))]
                pairwise_dists = squareform(pdist(self.descr_train[e],
                    constants.ml_metric[self.kernel]))
                logger.info("building kernel matrix of size (%d,%d); %7.4f Gbytes" \
                    % (size_training, size_training, 8*size_training**2/1e9))
                power  = constants.ml_power[self.kernel]
                prefac = constants.ml_prefactor[self.kernel]
                kmat = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
                kmat += self.krr_lambda*np.identity(len(self.target_train[e]))
                self.alpha_train[e] = np.linalg.solve(kmat,tgt_prop)
        logger.info("training of multipoles finished.")
        return None

    def predict_mol(self, _system, charge=0):
        '''Predict multipoles in local reference frame given descriptors.'''
        _system.initialize_multipoles()
        _system.compute_basis()
        _system.build_coulomb_matrices(self.max_neighbors)
        for e in self.alpha_train.keys():
            if self.alpha_train[e] is not None:
                pairwise_dists = cdist(_system.coulomb_mat, \
                        self.descr_train[e], constants.ml_metric[self.kernel])
                power  = constants.ml_power[self.kernel]
                prefac = constants.ml_prefactor[self.kernel]
                kmat = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
                pred = np.dot(kmat,self.alpha_train[e])
                for i in range(_system.num_atoms):
                    if _system.elements[i] == e:
                        _system.mtp_expansion[i] = pred[i]
        # Correct to get integer charge
        if self.calculator.Config.get("multipoles","correct_charge").lower() in \
            ['true','yes','1']:
            mol_mu = sum([1. if ele is "H" else 0 for ele in _system.elements])
            # abscharge = sum([abs(mtp[0]) for mtp in _system.mtp_expansion])
            totalcharge = sum([mtp[0] for mtp in _system.mtp_expansion])
            excess_chg = totalcharge - float(charge)
            if mol_mu > 0.:
                for i,mtp_i in enumerate(_system.mtp_expansion):
                    w_i = 1. if _system.elements[i] is "H" else 0
                    mtp_i[0] += -1.*excess_chg * (w_i/mol_mu)
        # Compute multipoles from basis set expansion
        _system.expand_multipoles()
        logger.debug("Predicted multipole expansion for %s" % ( _system))
        return None

    def add_mol_to_training(self, new_system, ref, atom=None):
        'Add molecule to training set'
        new_system.initialize_multipoles()
        new_system.build_coulomb_matrices(self.max_neighbors)
        new_system.multipoles = np.empty((new_system.num_atoms,9))
        # Read in multipole moments from text file
        new_system.load_mtp_from_ref(ref)

        for i in xrange(len(new_system.elements)):
            ele_i = new_system.elements[i]
            if ele_i is atom or atom is None:
                if ele_i not in self.target_train.keys():
                    self.target_train[ele_i] = []
                    self.descr_train[ele_i] = []
                    self.num_mols_train[ele_i] = 0
                new_target_train = []
                # Rotate system until atom pairs point in all x,y,z directions
                vec_all_dir = new_system.compute_basis()
                # Descriptor
                self.descr_train[ele_i].append(new_system.coulomb_mat[i])
                # charge
                new_target_train.append([new_system.multipoles[i][0]])
                # dipole
                new_target_train.append(np.dot(new_system.multipoles[i][1:4],
                                    new_system.basis[i].T))
                # quadrupole
                tmp = np.dot(np.dot(new_system.basis[i],
                    utils.spher_to_cart(new_system.multipoles[i][4:9])),
                            new_system.basis[i].T).reshape((9,))
                new_target_train.append(tmp)
                self.target_train[ele_i].append(new_target_train)
        if atom in new_system.elements or atom is None:
            self.num_mols_train[ele_i] += 1
        logger.info("Added file to training set: %s" % new_system)
        return None
